package obh.spring.proxy.app.v1;

public interface OrderServiceV1 {
    void orderItem(String itemId);
}
