package obh.spring.proxy.app.v1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping // @Controller 또는 @RequestMapping 있어야 스프링 컨트롤러로 인식
@ResponseBody
public interface OrderControllerV1 {

    @GetMapping("/proxy/v1/request")
    public String request(@RequestParam("itemId") String itemId);

    @GetMapping("/proxy/v1/no-log")
    public String noLog();

}
