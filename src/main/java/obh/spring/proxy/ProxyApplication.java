package obh.spring.proxy;

import obh.spring.proxy.config.AppV1Config;
import obh.spring.proxy.config.v1_proxy.ConcreteProxyConfig;
import obh.spring.proxy.config.v1_proxy.InterfaceProxyConfig;
import obh.spring.proxy.config.v2_dynamicproxy.DynamicProxyBasicConfig;
import obh.spring.proxy.config.v2_dynamicproxy.DynamicProxyFilterConfig;
import obh.spring.proxy.config.v3_proxyfactory.ProxyFactoryConfigV1;
import obh.spring.proxy.config.v3_proxyfactory.ProxyFactoryConfigV2;
import obh.spring.proxy.config.v4_postprocessor.BeanPostProcessorConfig;
import obh.spring.proxy.config.v5_autoproxy.AutoProxyConfig;
import obh.spring.proxy.config.v6_aop.AopConfig;
import obh.spring.proxy.trace.logtrace.LogTrace;
import obh.spring.proxy.trace.logtrace.ThreadLocalLogTrace;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

//@Import({AppV1Config.class, AppV2Config.class})
//@Import(InterfaceProxyConfig.class)
//@Import(ConcreteProxyConfig.class)
//@Import(DynamicProxyBasicConfig.class)
//@Import(DynamicProxyFilterConfig.class)
//@Import(ProxyFactoryConfigV1.class)
//@Import(ProxyFactoryConfigV2.class)
//@Import(BeanPostProcessorConfig.class)
//@Import(AutoProxyConfig.class)
@Import(AopConfig.class)
@SpringBootApplication(scanBasePackages = "obh.spring.proxy.app") //주의
public class ProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProxyApplication.class, args);
	}

	@Bean
	public LogTrace logTrace() {
		return new ThreadLocalLogTrace();
	}
}
