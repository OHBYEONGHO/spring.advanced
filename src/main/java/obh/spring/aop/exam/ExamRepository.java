package obh.spring.aop.exam;

import obh.spring.aop.exam.annotation.Retry;
import obh.spring.aop.exam.annotation.Trace;
import org.springframework.stereotype.Repository;

@Repository
public class ExamRepository {

    private static  int seq = 0;

    @Trace
    @Retry(value = 4)
    public String save(String itemId) {
        seq++;
        if (seq % 5 == 0) {
            throw new IllegalStateException("Transaction Exception");
        }
        return "Ok";
    }
}
