package obh.spring.aop;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "obh.spring.aop") //주의
public class AopApplication {
}
