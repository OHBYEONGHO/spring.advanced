package obh.spring.advanced.app.v1;

import lombok.RequiredArgsConstructor;
import obh.spring.advanced.trace.hellotrace.HelloTraceV1;
import obh.spring.advanced.trace.TraceStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceV1 {

    private final OrderRepositoryV1 orderRepositoryV1;
    private final HelloTraceV1 trace;

    public void orderItem(String itemId) {
        TraceStatus status = trace.begin("OrderServiceV1.orderItem()");
        try {
            orderRepositoryV1.save(itemId);
            trace.end(status);
        } catch (Exception e) {
            trace.exception(status, e);
            throw e;
        }
    }
}
