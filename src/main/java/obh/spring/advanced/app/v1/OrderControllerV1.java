package obh.spring.advanced.app.v1;

import lombok.RequiredArgsConstructor;
import obh.spring.advanced.trace.hellotrace.HelloTraceV1;
import obh.spring.advanced.trace.TraceStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OrderControllerV1 {

    private final OrderServiceV1 orderServiceV1;
    private final HelloTraceV1 trace;

    @RequestMapping("/v1/request")
    public String request(String itemId) {
        TraceStatus status = trace.begin("OrderControllerV1.request()");
        try {
            orderServiceV1.orderItem(itemId);
            trace.end(status);
            return "ok";
        } catch (Exception e) {
            trace.exception(status, e);
            throw e;
        }
    }
}
