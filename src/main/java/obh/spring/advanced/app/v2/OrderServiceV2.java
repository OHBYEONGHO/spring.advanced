package obh.spring.advanced.app.v2;

import lombok.RequiredArgsConstructor;
import obh.spring.advanced.trace.TraceId;
import obh.spring.advanced.trace.TraceStatus;
import obh.spring.advanced.trace.hellotrace.HelloTraceV2;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceV2 {

    private final OrderRepositoryV2 orderRepositoryV2;
    private final HelloTraceV2 trace;

    public void orderItem(TraceId traceId, String itemId) {
        TraceStatus status = trace.beginSync(traceId, "OrderServiceV2.orderItem()");
        try {
            orderRepositoryV2.save(status.getTraceId(), itemId);
            trace.end(status);
        } catch (Exception e) {
            trace.exception(status, e);
            throw e;
        }
    }
}
