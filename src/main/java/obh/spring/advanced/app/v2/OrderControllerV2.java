package obh.spring.advanced.app.v2;

import lombok.RequiredArgsConstructor;
import obh.spring.advanced.trace.TraceStatus;
import obh.spring.advanced.trace.hellotrace.HelloTraceV2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OrderControllerV2 {

    private final OrderServiceV2 orderServiceV2;
    private final HelloTraceV2 trace;

    @RequestMapping("/v2/request")
    public String request(String itemId) {
        TraceStatus status = trace.begin("OrderControllerV2.request()");
        try {
            orderServiceV2.orderItem(status.getTraceId(), itemId);
            trace.end(status);
            return "ok";
        } catch (Exception e) {
            trace.exception(status, e);
            throw e;
        }
    }
}
