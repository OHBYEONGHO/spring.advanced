package obh.spring.advanced.app.v4;

import lombok.RequiredArgsConstructor;
import obh.spring.advanced.trace.TraceStatus;
import obh.spring.advanced.trace.logtrace.LogTrace;
import obh.spring.advanced.trace.template.AbstractTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OrderControllerV4 {

    private final OrderServiceV4 orderServiceV4;
    private final LogTrace trace;

    @RequestMapping("/v4/request")
    public String request(String itemId) {

        AbstractTemplate<String> template = new AbstractTemplate<String>(trace) {
            @Override
            protected String call() {
                orderServiceV4.orderItem(itemId);
                return "ok";
            }
        };
        return template.execute("OrderControllerV4.request()");
    }
}
