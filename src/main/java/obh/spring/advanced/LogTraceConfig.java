package obh.spring.advanced;

import obh.spring.advanced.trace.logtrace.FieldLogTrace;
import obh.spring.advanced.trace.logtrace.LogTrace;
import obh.spring.advanced.trace.logtrace.ThreadLocalLogTrace;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogTraceConfig {

    @Bean
    public LogTrace logTrace() {
//        return new FieldLogTrace();
        return new ThreadLocalLogTrace();
    }
}
