package obh.spring.proxy.pureproxy.decorator.code;

public interface Component {
    String operation();
}
