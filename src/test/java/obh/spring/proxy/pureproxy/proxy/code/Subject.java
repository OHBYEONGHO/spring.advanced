package obh.spring.proxy.pureproxy.proxy.code;

public interface Subject {
    String operation();
}
