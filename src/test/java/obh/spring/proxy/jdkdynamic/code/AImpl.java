package obh.spring.proxy.jdkdynamic.code;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AImpl implements AInterface {
    @Override
    public String call() {
        log.info("A 호출");
        return "a";
    }

    @Override
    public String call2() {
        log.info("A 두번째 메소드 호출");
        return "aa";
    }
}

