package obh.spring.proxy.common.service;

public interface ServiceInterface {
    void save();
    void find();
}
