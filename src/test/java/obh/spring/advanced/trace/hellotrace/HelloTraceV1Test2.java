package obh.spring.advanced.trace.hellotrace;

import obh.spring.advanced.trace.TraceStatus;
import obh.spring.advanced.trace.hellotrace.HelloTraceV2;
import org.junit.jupiter.api.Test;

class HelloTraceV1Test2 {

    @Test
    void begin_end() {
        HelloTraceV2 trace = new HelloTraceV2();
        TraceStatus status1 = trace.begin("begin");
        TraceStatus status2 = trace.beginSync(status1.getTraceId(), "beginSync");

        trace.end(status2);
        trace.end(status1);
    }

    @Test
    void begin_exception() {
        HelloTraceV2 trace = new HelloTraceV2();
        TraceStatus status1 = trace.begin("begin");
        TraceStatus status2 = trace.beginSync(status1.getTraceId(), "beginSync");

        trace.exception(status2, new IllegalStateException());
        trace.exception(status1, new IllegalStateException());
    }

    @Test
    void exception() {
    }
}