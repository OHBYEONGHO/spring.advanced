package obh.spring.advanced.trace.hellotrace;

import obh.spring.advanced.trace.TraceStatus;
import obh.spring.advanced.trace.hellotrace.HelloTraceV1;
import org.junit.jupiter.api.Test;

class HelloTraceV1Test {

    @Test
    void begin_end() {
        HelloTraceV1 traceV1 = new HelloTraceV1();
        TraceStatus status = traceV1.begin("begin");
        traceV1.end(status);
    }

    @Test
    void begin_exception() {
        HelloTraceV1 traceV1 = new HelloTraceV1();
        TraceStatus status = traceV1.begin("begin");
        traceV1.exception(status, new IllegalStateException());
    }

    @Test
    void exception() {
    }
}